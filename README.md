# imsdata-web

Tiny PHP web app for visualisation and retrieving of energetics data from intelligent meters (IMS) of Slovak energy providers via IMS portals. 


![app screenshot](https://cdn.mcms.sk/img/imsdata-web-thumb.jpg)


# Installation:

1. Create database for app and import tables + procedure from repository file:  sql/database.sql
2. Edit database parameters and IMS login info in config file located in:  src/config.json
3. Move all from src directory to your web directory and point DOCUMENT_ROOT into app's "htdocs" directory
4. Set cronjob to web path:  your-app-url.tld/cron/retrieveSSEdData

# Requirements:

1. PHP 5.6+
2. MySQL/MariaDB
3. Apache server + mod_rewrite + .htaccess 


# Supported:

## 1. Stredoslovenská Energetika - Distribúcia (SSEd):  IMS portal
[https://ims.sse-d.sk]

