<?php
namespace SSEdPortal\Classes;

class SSEdPortal extends cURL {

	private $urlLoginForm = "https://ims.sse-d.sk/logon";
	private $urlLogin = "https://ims.sse-d.sk/logon/?redirect=/consumption-production/profile-data/";
	private $urlProfileData = "https://ims.sse-d.sk/consumption-production/profile-data/";
	private $screenNoLogin = "AC.001";
	private $screenNoProfileData = "CP.015";
	private $verificationToken = null;

	public function __construct($proxy=null)
	{
		parent::__construct();
		if($proxy) $this->proxy = $proxy;
	}

	public function getProfileData(\DateTime $dateFrom, \DateTime $dateTo)
	{
		// we need load page first for parse verification token for post request
		$this->get($this->urlProfileData);
		
		$pointOfDeliveryId = $this->getPointOfDeliveryId($this->last_html);
		$verificationToken = $this->getVerificationToken($this->last_html);
		
		$page = 0;
		$limit = 96;
		$total = 1;
		
			
		$data = array();
		while($limit*$page <= $total)
		{
			$response = $this->post($this->urlProfileData, 
							   sprintf("orderBy=MeteringDatetime-asc&page=%d&ProfileDataFilter.IncludeCustom=False&ProfileDataFilter.IncludeTerminated=False&ProfileDataFilter.PointOfDeliveryId=%s&ProfileDataFilter.ValidFromDate=%s&ProfileDataFilter.ValidToDate=%s&size=%d", 
									   $page, $pointOfDeliveryId, $dateFrom->format("d. m. Y"), $dateTo->format("d. m. Y"), $limit),
							   [ "__screenNumber: ".$this->screenNoProfileData, "__RequestVerificationToken: ".$verificationToken, "X-Requested-With: XMLHttpRequest" ]);
		
			$json = null;
			if($response)
			{
				$json = json_decode($response);
				if(!empty($json) && !empty($json->data))
				{
					if(!empty($json->total)) $total = $json->total;
					
					$data = array_merge($data, $json->data);
				}
			}
			else {
				echo "error: no data on page ".($page+1)."<br>";
				echo $this->request_info;
			}
			
			$page++;
			if($page>30) die("terminated - pages limit exceeded (page: ".$page." | total: ".ceil($total/$limit).")<br>");
			usleep(rand(3,9)*100000); // micro seconds (300-900 ms)
		}
		
		return $data;
	}
	
	public function login(string $login, string $pass)
	{
		$this->get($this->urlLoginForm);
		
		return $this->post($this->urlLogin, sprintf("__RequestVerificationToken=%s&__screenNumber=%s&Password=%s&UserName=%s", 
												 $this->getVerificationToken($this->last_html), $this->screenNoLogin, $pass, $login),
												 [ "Referer: ".$this->urlLoginForm ]);
	}
	
	protected function getPointOfDeliveryId(string $rawHtml)
	{
		return preg_replace("/^.+<input[^>]+name=\"ProfileDataFilter.PointOfDeliveryId\"[^>]+value=\"([0-9]+)\".+$/Uis","$1",$rawHtml);
	}

	protected function getVerificationToken(string $rawHtml)
	{
		return preg_replace("/^.+<input[^>]+name=\"__RequestVerificationToken\"[^>]+value=\"([^\"]+)\".+$/Uis","$1",$rawHtml);
	}
	
}

?>
