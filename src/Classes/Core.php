<?php 
namespace SSEdPortal\Classes;

use SSEdPortal\Classes\MysqliWrapper;

class Core { 
    
    private static $db;
    
    public static function dbInit($host,$user,$pass,$db)
    {
        static::$db = new MysqliWrapper($host,$user,$pass,$db);
    }

    public static function db()
    {
        return static::$db;
    }

    public static function loadController($name)
    {
        $controllerName = "Page";
        
        if(!empty($name) && file_exists(static::getControllerFilename($name)))
        {
            $controllerName = $name;
        }
        
        require_once static::getControllerFilename($controllerName);
        
        $controllerNS = sprintf("\\SSEdPortal\\Controllers\\%sController", $controllerName);
        return new $controllerNS();
    }
    
    public static function loadMethod($controller, $method)
    {
        $ctrl = static::loadController($controller);
        
        if(method_exists($ctrl, $method)) return $ctrl->$method();
        else static::error(404);
    }
    
    public static function view($name)
    {
        if(file_exists(static::getViewFilename($name)))
        {
            $html  = file_get_contents(static::getViewFilename("_header"));
            $html .= file_get_contents(static::getViewFilename($name));
            $html .= file_get_contents(static::getViewFilename("_footer"));
            
            return $html;
        }
        return static::error(500,"Template with name ". htmlspecialchars($name)." not found.");
    }
    
    public function json($data,$print=false) {
        $data = json_encode($data);
        
        if($print)
        {
            header("Content-type: text/json; charset=utf-8");
            echo $data;
            exit;
        }
        else return $data;
    }
    
    
    public static function getControllerFilename($name)
    {
        return sprintf("%sControllers/%sController.php", ROOT_PATH, ucfirst($name));
    }
    public static function getViewFilename($name)
    {
        return sprintf("%sViews/%s.html", ROOT_PATH, strtolower($name));
    }
    
    
    public static function error($code,$msg=null)
    {
        switch($code)
        {
            case 401:
                header("HTTP/1.0 401 Unauthorized");
                break;
            
            case 403:
                header("HTTP/1.0 403 Forbidden");
                break;
            
            case 404:
                header("HTTP/1.0 404 Not Found");
                break;
            
            default:
                header("HTTP/1.0 500 Internal Server Error");
                break;
        }
        
        die("ERROR ".$code.(!empty($msg) ? ": ".$msg : ""));
    }
} 
?>
