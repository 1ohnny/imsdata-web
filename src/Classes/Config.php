<?php 
namespace SSEdPortal\Classes;

class Config { 
    static $CFG = null;
  
    public static function get($key)
    {
        if(!empty(static::$CFG))
        {
            foreach(static::$CFG as $ck=>$cv)
            {
                if($ck==$key) return $cv;
            }
        }
        
        return null;
    }
    
    public static function load($path)
    {
        static::$CFG = json_decode(file_get_contents($path), true);
    }
} 
?>
