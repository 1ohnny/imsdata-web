<?php 
namespace SSEdPortal\Classes;

class Curl { 
  var $headers; 
  var $user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0"; 
  var $compression; 
  var $cookie_file; 
  var $proxy; 
  var $return_headers = 0;
  var $last_html;
  var $request_info;
  
  function __construct($cookies=TRUE,$cookiesFile='cookies.txt',$compression='gzip',$proxy='') { 
    $this->compression=$compression; 
    $this->proxy=$proxy; 
    $this->cookies=$cookies; 
    if ($this->cookies == TRUE) $this->cookie($cookiesFile); 
  } 
  
  function setHeaders($adtHeaders=null)
  {
    $this->headers = array();
	$this->headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'; 
	$this->headers[] = 'Accept-Encoding: gzip, deflate, br';
	$this->headers[] = 'Accept-Language: en-US,en;q=0.5';
	$this->headers[] = 'Cache-Control: max-age=0';
    $this->headers[] = 'Connection: keep-alive'; 
    
    if(!empty($adtHeaders))
    {
    	foreach($adtHeaders as $hdr) $this->headers[] = $hdr;
    }
  }
  
  function cookie($cookie_file) { 
	if (file_exists($cookie_file)) { 
		$this->cookie_file=$cookie_file;
		file_put_contents($cookie_file, ""); 
	} else { 
		$fp = fopen($cookie_file,'w+') or die('The cookie file could not be opened. Make sure this directory has the correct permissions'); 
		$this->cookie_file = $cookie_file; 
		fclose($fp); 
	}
  } 
  
  function get($url, $adtHeaders=null) {
	$this->setHeaders($adtHeaders);

	$process = curl_init($url); 
	curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers); 
	curl_setopt($process, CURLOPT_HEADER, $this->return_headers); 
	curl_setopt($process, CURLOPT_USERAGENT, $this->user_agent); 
	if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEFILE, $this->cookie_file); 
	if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEJAR, $this->cookie_file); 
	curl_setopt($process,CURLOPT_ENCODING , $this->compression); 
	curl_setopt($process, CURLOPT_TIMEOUT, 20); 
	if ($this->proxy) curl_setopt($process, CURLOPT_PROXY, $this->proxy); 
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0); 
    curl_setopt($process, CURLOPT_VERBOSE, 0); 
    curl_setopt($process, CURLINFO_HEADER_OUT, 1);
    curl_setopt($process, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($process, CURLOPT_SSL_VERIFYPEER, true);
	
	$this->last_html = curl_exec($process);
	$this->request_info = curl_getinfo($process,CURLINFO_HEADER_OUT);
	
	curl_close($process); 
	return $this->last_html; 
  } 
  
  function post($url,$data, $adtHeaders=null) {
  	$this->setHeaders($adtHeaders);
  	$this->headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8'; 
  
    $process = curl_init($url); 
    curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers); 
    curl_setopt($process, CURLOPT_HEADER, $this->return_headers); 
    curl_setopt($process, CURLOPT_USERAGENT, $this->user_agent); 
    if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEFILE, $this->cookie_file); 
    if ($this->cookies == TRUE) curl_setopt($process, CURLOPT_COOKIEJAR, $this->cookie_file); 
    curl_setopt($process, CURLOPT_ENCODING , $this->compression); 
    curl_setopt($process, CURLOPT_TIMEOUT, 20); 
    if ($this->proxy) curl_setopt($process, CURLOPT_PROXY, $this->proxy); 
    curl_setopt($process, CURLOPT_POSTFIELDS, $data); 
    curl_setopt($process, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($process, CURLOPT_VERBOSE, 0); 
    curl_setopt($process, CURLOPT_POST, 1); 
    curl_setopt($process, CURLINFO_HEADER_OUT, 1);
    curl_setopt($process, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($process, CURLOPT_SSL_VERIFYPEER, true);

	$this->last_html = curl_exec($process);
	$this->request_info = curl_getinfo($process,CURLINFO_HEADER_OUT);

    curl_close($process); 
    return $this->last_html; 
  } 
  
} 
?>
