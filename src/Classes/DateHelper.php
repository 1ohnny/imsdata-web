<?php
namespace SSEdPortal\Classes;

class DateHelper {
    
    private static $weekDays = [0=>"Ne", "Po", "Ut", "St", "Št", "Pi", "So"];
    private static $monthNames = [1=>"Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"];
    
    
    public static function getLastMonths($count,$monthNames=false)
    {
        $arr = [];
        
        $dt = new \DateTime();
        for($i=0; $i<$count; $i++)
        {
            $arr[$dt->format("Y-m")] = sprintf("%s %s", static::$monthNames[$dt->format("n")], $dt->format("Y"));
            
            $dt->modify("-1 month");
        }
        
        return $arr;
    }
    
    public static function getLastMonthsOptions($count,$monthNames=false)
    {
        return static::getOptions(static::getLastMonths($count, $monthNames));
    }
	
    public static function getMonthDays($month)
    {
        $arr = [];
        
        $dt = new \DateTime($month."-01");
        $maxDay = $dt->format("t");
        if($dt->format("Ym")==date("Ym")) $maxDay = date("j")-1;
        $dt->modify("+".($maxDay-1)." day");
        //die($dt->format("Ymd"));
        for($i=$maxDay; $i>=1; $i--)
        {
            $arr[$dt->format("d")] = sprintf("%s %s.", static::$weekDays[$dt->format("w")], $dt->format("d"));
            
            $dt->modify("-1 day");
        }
        
        return $arr;
    }
    
    public static function getMonthDaysOptions($month,$current=null,$lastDay=false)
    {
        return static::getOptions(static::getMonthDays($month),$current, $lastDay);
    }
    
    
    public static function getOptions($arr,$current=null)
    {
        $html = "";
        
        foreach($arr as $k=>$n)
        {
            $html .= sprintf('<option value="%s"%s>%s</option>', $k, (!empty($current) && $k==$current ? ' selected="selected"':''), $n);
        }
        
        return $html;
    }
}

?>
