<?php
namespace SSEdPortal\Classes;

class MysqliWrapper
{
	private $conn;
        private $host;
        private $user;
        private $pass;
        private $db;


        public function __construct($host,$user,$pass,$db) {
            $this->host = $host;
            $this->user = $user;
            $this->pass = $pass;
            $this->db   = $db;
        }
	
	public function connect()
	{
            $this->conn = mysqli_connect($this->host,$this->user,$this->pass,$this->db);
	}

        public function truncate($table)
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            return mysqli_query($this->conn, sprintf("TRUNCATE TABLE %s",$table));
        }
        
        public function fetchAll($query,$params=null) 
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            if(!empty($params)) $query = $this->formatQuery($query, $params);
            
            $res = mysqli_query($this->conn, $query);
            return mysqli_fetch_all($res, MYSQLI_ASSOC);
        }
        
        public function fetchSingle($query,$params=null) 
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            if(!empty($params)) $query = $this->formatQuery($query, $params);
            
            $res = mysqli_query($this->conn, $query);
            return mysqli_fetch_row($res);
        }
        
        public function execute($procedure) 
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            
            return mysqli_query($this->conn, sprintf("CALL %s",$procedure));
        }
        
        public function batchInsert($query,$colsCfg,$data) 
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            $dataChunk = array();
            for($i=0; $i<count($data); $i++)
            {
                $dataChunk[] = $this->getColsValues($colsCfg, $data[$i]);
                
                if($i>0 && ($i%100)==0)
                {
                    $res = $this->batchInsertSave($query, $dataChunk);
                    if(!$res) die(mysqli_error($this->conn));
                    $dataChunk = array();
                }
            }
            if(count($dataChunk)>0) {
                    
                $res = $this->batchInsertSave($query, $dataChunk);
                if(!$res) die(mysqli_error($this->conn));
            }
            
            return true;
        }
        
        
        private function formatQuery($query,$data)
        {
            foreach($data as $k=>$v)
            {
                $query = str_replace($k,sprintf("'%s'",$this->escape($v)), $query);
            }
            
            return $query;
        }
        
        public function escape($str)
        {
            if(empty($this->conn)) {
                $this->connect();
            }
            
            return mysqli_real_escape_string($this->conn, $str);
        }
        
        private function batchInsertSave($query, $dataChunk)
        {
            return mysqli_query($this->conn, sprintf("%s VALUES (%s);", $query, join("),(",$dataChunk)));
        }
        
        private function getColsValues($colsCfg,$item)
        {
            $cols = array();
            foreach($colsCfg as $col)
            {
                $cols[] = $item->{$col};
            }
            
            return sprintf("'%s'",join("','",$cols));
        }
}
