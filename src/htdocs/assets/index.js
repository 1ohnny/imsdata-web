$(document).ready(function() {
    LoadGrid();
    
    $(".filter-prop").on("change", DatatableFilterChange);
});

var CURRENT_GRAPH;
function LoadGraph(data)
{
    if(data.length==0)
    {
        $("#graphArea").hide();
        $("#graphAreaEmpty").show();
        return;
    }
    else {
        $("#graphAreaEmpty").hide();
        $("#graphArea").show();
    }
    
    data = GraphPrepareData(data);
    
    $.jqplot.config.enablePlugins = true;
    var options = {
        grid: {
            drawGridLines: true,        // wether to draw lines across the grid or not.
            gridLineColor: '#ddd',   // *Color of the grid lines.
            background: '#FFFFFF',      // CSS color spec for background color of grid.
            borderColor: '#E7E7E7',     // CSS color spec for border around grid.
            borderWidth: 2.0,           // pixel width of border around grid.
            shadow: false,               // draw a shadow for grid.
        },
        seriesDefaults: { rendererOptions: { shadowOffset: 0 }, markerOptions: { shadow: false }, showMarker: false },
        seriesColors: ["#ea304e","#3060ea"],
        axes: {
            xaxis:{ label: "deň", labelRenderer: $.jqplot.CanvasAxisLabelRenderer, labelOptions: { fontSize: '8pt' }, tickInterval: 1, min: 1, max: getMonthLastDay(), tickOptions: { formatString: '%d', formatter: function(format, value) { return "<div class=\"text-center\">"+value+"<br>"+GraphShortdayFormatter(value)+"</div>"; } } },
            yaxis:{ label: "spotreba [kWh]", labelRenderer: $.jqplot.CanvasAxisLabelRenderer, labelOptions: { fontSize: '8pt' }, min: 0, max: getMaxValue(data), tickInterval: 5 }
        },
        highlighter: {
            sizeAdjust: 10,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: "<span style=\"padding: 4px 6px; display: block; font-size: 110%; color: #ea304e;\">Spotreba [kWh]: <b>%s</b></span>",
            tooltipOffset: 15,
            useAxesFormatters: false
        },
        cursor: {
            show: false
        }
    };
    
    if(CURRENT_GRAPH) {
        CURRENT_GRAPH.destroy();
    }
    CURRENT_GRAPH = $.jqplot("graphArea", [ data ], options);
}

function GraphPrepareData(data)
{
    var values = [];
    
    for(var x in data)
    {
        values[parseInt(DayFormatter(data[x].measure_date_start))-1] = data[x].value;  
    }
    
    return values;
}

var SHORTDAYS_SK = ["Ne","Po","Ut","St","Št","Pi","So"];
function GraphShortdayFormatter(day)
{
    var dt = new Date($("#filterMonth").val()+"-"+day);
    return SHORTDAYS_SK[dt.getDay()];
}

function LoadGrid()
{
    LoadDatatable("getDaysData", [
        {
            "targets": 0,
            "data": "id",
            "visible": false
        },
        {
            "targets": 1,
            "data": "measure_date",
            "render": DateFormatter,
            "class": "text-center",
            "orderable": false
        },
        {
            "targets": 2,
            "data": "value",
            "class": "text-center",
            "orderable": false,
        }
    ], {
        "sDom": "t",
        "customCallback": function(res) { LoadGraph(res.data); },
        scrollY: '50vh',
        scrollCollapse: true,
    });
}

function getMonthLastDay()
{
    var dt = new Date($("#filterMonth").val()+"-01");
    return getLastDOM(dt).getDate();
}

function getMaxValue(data)
{
    var max = 30;
    for(x in data)
    {
        if(data[x]>max) max = Math.round(data[x])+3;
    }
    return max;
}