<?php
namespace SSEdPortal;

define("ROOT_PATH","../");

require ROOT_PATH."Classes/Core.php";
require ROOT_PATH."Classes/Config.php";
require ROOT_PATH."Classes/DateHelper.php";
require ROOT_PATH."Classes/Curl.php";
require ROOT_PATH."Classes/MysqliWrapper.php";
require ROOT_PATH."Classes/SSEdPortal.php";

use SSEdPortal\Classes\Core;
use SSEdPortal\Classes\Config;

///////////////////////////////////////////

// load config
Config::load(ROOT_PATH."config.json");

// init db
Core::dbInit(Config::get("db_host"),Config::get("db_user"),Config::get("db_pass"),Config::get("db_name"));

// routing
if(empty($_GET["ctrl"])) $_GET["ctrl"] = "page";
if(empty($_GET["method"])) $_GET["method"] = "index";

echo Core::loadMethod($_GET["ctrl"],$_GET["method"]);
?>
