<?php
namespace SSEdPortal\Controllers;

use SSEdPortal\Classes\Core;
use SSEdPortal\Classes\DateHelper;

/**
 * Description of ApiController
 *
 * @author Jan Smatlik <jan@trinetus.com>
 */
class ApiController {
    
    public function getDaysData()
    {
        $f = $this->parseGridFilter();
        $m = (empty($f["month"]) ? date("Y-m") : $f["month"]);
        $dt = new \DateTime(sprintf("%s-01",$m));
        
        $sql = 'SELECT id, measure_date, measure_date_start, SUM(value/4) as value FROM profile_data WHERE cast(measure_date_start as date) >= :dateStart AND cast(measure_date_start as date) <= :dateEnd GROUP BY day(measure_date_start)';
        
        $data = Core::db()->fetchAll($sql, [":dateStart" => $dt->format("Y-m-d"), ":dateEnd" => $dt->format("Y-m-t")]);
        
        return Core::json(["data" => $data],true);
    }
    
    public function getHoursData()
    {
        $f = $this->parseGridFilter();
        $date = (new \DateTime())->modify("-1 days")->format("Y-m-d");
        if(!empty($f["day"]) && !empty($f["month"])) $date = sprintf("%s-%s",$f["month"],$f["day"]);
        
        $sql = 'SELECT id, measure_date, measure_date_start, SUM(value/4) as value FROM profile_data pd WHERE cast(measure_date_start as date) = :dateStart GROUP BY day(measure_date_start), hour(measure_date_start)';
        
        $data = Core::db()->fetchAll($sql, [":dateStart" => $date]);
        
        $dateObj = new \DateTime($date);
        if(empty($f["day"]) || empty($f["month"])) $dateObj->modify("-1 days");
        return Core::json(["data" => $data, "dataDays" => DateHelper::getMonthDaysOptions($dateObj->format("Y-m"), $dateObj->format("d"))],true);
    }
    
    
    
    private function parseGridFilter()
    {
        if(!empty($_GET["filter"]))
        {
            $f = json_decode($_GET["filter"],true);
            if(!empty($f["month"])) return $f;
        }
        return null;
    }
}
