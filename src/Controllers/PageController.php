<?php
namespace SSEdPortal\Controllers;

use SSEdPortal\Classes\Core;
use SSEdPortal\Classes\DateHelper;

/**
 * Description of PageController
 *
 * @author Jan Smatlik <jan@trinetus.com>
 */
class PageController {
    
    public function index()
    {
        $out = Core::view("index");
        
        $out = str_replace("{monthsOptions}", DateHelper::getLastMonthsOptions(12, true), $out);
        
        return $out;
    }
    
    public function hours()
    {
        $out = Core::view("hours");
        
        $out = str_replace(array("{monthsOptions}","{daysOptions}"), 
                           array(DateHelper::getLastMonthsOptions(12, true),DateHelper::getMonthDaysOptions(date("Y-m"), date("d"), true)), 
                           $out);
        
        return $out;
    }
}
