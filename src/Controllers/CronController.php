<?php
namespace SSEdPortal\Controllers;

use SSEdPortal\Classes\Core;
use SSEdPortal\Classes\Config;
use SSEdPortal\Classes\SSEdPortal;

/**
 * Description of CronController
 *
 * @author Jan Smatlik <jan@trinetus.com>
 */
class CronController {
    
    public function retrieveSSEdData()
    {
        $crw = new SSEdPortal();
        $crw->login(Config::get("login"), Config::get("password"));

        if(!empty($_GET["from"]) && !empty($_GET["to"])) {
            $dtFrom = new \DateTime($_GET["from"]);
            $dtTo = new \DateTime($_GET["to"]);
        }
        else {
            $dtFrom = new \DateTime();
            $dtFrom->modify("-3 days");
            $dtTo = new \DateTime();
        }
        
        echo "Data from: ".$dtFrom->format("Y-m-d")." to: ".$dtTo->format("Y-m-d")."<br>";
        $data = $crw->getProfileData($dtFrom, $dtTo);
        if($data)
        {
            foreach($data as $idx=>$item)
            {
                $data[$idx]->MeasureDate = date("Y-m-d H:i:s",$item->MeteringDatetime->Value/1000);
                $data[$idx]->StartPeriod = date("Y-m-d H:i:s",$item->StartPeriodDatetime->Value/1000);
            }

            echo "data count: ".count($data)."<br>\n";
            
            Core::db()->truncate("profile_data_import");

            Core::db()->batchInsert("INSERT INTO profile_data_import (measure_date,measure_date_start,period,value,quality)", 
                                         [ "MeasureDate", "StartPeriod", "Period", "ActualConsumption", "ActualConsumptionQualityTypeCode" ], 
                                         $data);

            Core::db()->execute("p_processProfileData()");

            echo "finished<br>";
        }
        else echo "ERR: no data";
    }
}
