SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP PROCEDURE IF EXISTS `p_processProfileData`;;
CREATE PROCEDURE `p_processProfileData`()
BEGIN 

UPDATE profile_data pd 
INNER JOIN profile_data_import pdi ON pd.measure_date = pdi.measure_date
SET pd.value = pdi.value, pd.quality = pdi.quality;

INSERT INTO profile_data (measure_date, measure_date_start, period, value, quality)
SELECT pdi.measure_date, pdi.measure_date_start, pdi.period, pdi.value, pdi.quality
FROM profile_data_import pdi 
LEFT JOIN profile_data pd ON pd.measure_date = pdi.measure_date
WHERE pd.id IS NULL
GROUP BY pdi.measure_date;

END;;

DELIMITER ;

CREATE TABLE `profile_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `measure_date` datetime NOT NULL,
  `measure_date_start` datetime NOT NULL,
  `period` int(8) unsigned NOT NULL,
  `value` decimal(8,3) unsigned NOT NULL,
  `quality` tinyint(1) unsigned NOT NULL COMMENT '1=measured,2=replaced',
  PRIMARY KEY (`id`),
  UNIQUE KEY `measure_date` (`measure_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;


CREATE TABLE `profile_data_import` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `measure_date` datetime NOT NULL,
  `measure_date_start` datetime NOT NULL,
  `period` int(8) unsigned NOT NULL,
  `value` decimal(8,3) unsigned NOT NULL,
  `quality` tinyint(1) unsigned NOT NULL COMMENT '1=measured,2=replaced',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;



